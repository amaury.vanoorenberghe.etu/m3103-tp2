package tp2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ListUtils {
	
	
	public static List<Integer> genereRdmIntList() {
		return IntStream
			.range(10, 100)
			.limit(30)
			.boxed()
			.collect(Collectors.toList());
	}
	
	public static void affiche(List<Integer> l) {
		System.out.println(
			l
			.stream()
			.map(x -> x.toString())
			.collect(Collectors.joining(" -> "))
		);
		
		/*Iterator<Integer> li = l.iterator();
		
		if (li.hasNext()) {
			System.out.print(li.next());
		}
		
		while (li.hasNext()) {
			System.out.print(" -> " + li.next());
		}
		
		System.out.println();*/
	}
	
	public static void afficheInverse(List<Integer> l) {
		ListIterator<Integer> li = l.listIterator(l.size());
		
		if (li.hasPrevious()) {
			System.out.print(li.previous());
		}
		
		while (li.hasPrevious()) {
			System.out.print(" -> " + li.previous());
		}
		
		System.out.println();
	}
	
	public static int somme(List<Integer> l) {
		return l
			.stream()
			.reduce((x,  y) -> x + y)
			.get();
	}
	
	public static int moyenne(List<Integer> l) {
		return somme(l) / l.size();
	}
	
	public static int max(List<Integer> l) {
		return Collections.max(l);
	}
	
	public static int min(List<Integer> l) {
		return Collections.min(l);
	}
	
	public static List<Integer> positions(List<Integer> l, int n) {
		/*return IntStream.range(0, l.size())
			.filter(idx -> l.get(idx) == n)
			.boxed()
			.collect(Collectors.toList());*/
		
		Iterator<Integer> li = l.iterator();
		List<Integer> r = new ArrayList<Integer>();
		int idx = 0;
		
		while (li.hasNext()) {
			if (li.next() == n) {
				r.add(idx);
			}
			
			++idx;
		}
		
		return r;
	}
	
	public static List<Integer> paire(List<Integer> l) {
		return l.parallelStream()
			.filter(x -> x % 2 == 0)
			.collect(Collectors.toList());
	}
	
	public static boolean estTrie(List<Integer> l) {
		/*return IntStream.range(0, l.size() - 1)
			.allMatch(idx -> l.get(idx) < l.get(idx + 1));*/
		
		Iterator<Integer> li = l.iterator();
		
		if (!li.hasNext()) {
			return true;
		}
		
		int previous = li.next();
		int current;
		
		while (li.hasNext()) {
			current = li.next();
			
			if (current < previous) {
				return false;
			}
			
			previous = current;
		}
		
		return true;
	}
	
	public static List<Integer> trie(List<Integer> l) {
		return l
			.stream()
			.sorted()
			.collect(Collectors.toList());
	}
}
