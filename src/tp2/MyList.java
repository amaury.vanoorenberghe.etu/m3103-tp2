package tp2;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class MyList<E> implements Iterable<E> {
	protected E element;
	protected MyList<E> previous, next;
	
	public MyList() {
		this(null, null, null);
	}
	
	public MyList(E element, MyList<E> previous, MyList<E> next) {
		this.element = element;
		this.previous = previous;
		this.next = next;
	}

	public boolean isEmpty() {
		return element == null;
	}
	
	public void clear() {		
		element = null;	
		next = null;
	}
	
	public boolean add(E item) {
		MyList<E> here = last();
		
		if (here.isEmpty()) {
			here.element = item;
		} else {
			here.next = new MyList<E>(item, here, null);		
		}
		
		return true;
	}
	
	public void add(int index, E item) {
		MyList<E> here = subset(index);
		
		if (here.isEmpty()) {
			here.element = item;
			return;
		}
		
		MyList<E> newNext = new MyList<E>(here.element, here, here.next);
		
		here.next = newNext;
		
		here.element = item;
	}
	
	public E remove(int index) {
		MyList<E> here = subset(index);
				
		E deleted = here.element;
		
		if (here.hasNext()) {
			here.element = here.next.element;
			here.next = here.next.next;
			return deleted;
		}

		here.element = null;
		
		if (here.hasPrevious()) {
			here.previous.next = null;
			return deleted;
		}
		
		return deleted;
	}
	
	public boolean remove(Object o) {
		int index = indexOf(o);
		
		if (index >= 0) {
			remove(index);
			return true;
		}
		
		return false;
	}
	
	public int size() {
		if (isEmpty()) {
			return 0;
		}
		
		MyList<E> here = this;
		int count = 1;
		
		while (here.hasNext() && !here.next.isEmpty()) {
			here = here.next;
			++count;
		}
		
		return count;
	}
	
	public E get(int index) {
		return subset(index).element;
	}
	
	private MyList<E> subset(int index) {
		if (index < 0) {
			throw new IndexOutOfBoundsException();
		}
		
		MyList<E> here = this;
		
		for (int i = 0; i < index; ++i) {
			if (!here.hasNext()) {
				throw new IndexOutOfBoundsException();
			}
			
			here = here.next;
		}
		
		if (here.isEmpty()) {
			throw new IndexOutOfBoundsException();
		}
		
		return here;
	}
	
	public int indexOf(Object o) {
		MyList<E> here = this;
		int index = 0;
		
		while (here != null && !here.isEmpty() ) {
			if (here.element.equals(o)) {
				return index;
			}
			
			here = here.next;
			++index;
		}
		return -1;
	}
	
	public boolean contains(Object o) {
		return indexOf(o) != -1;
	}
	
	public int lastIndexOf(Object o) {
		MyList<E> here = last();
		int index = size() - 1;
		
		while (here != null && !here.isEmpty() ) {
			if (here.element.equals(o)) {
				return index;
			}
			
			here = here.previous;
			--index;
		}
		
		return -1;
	}
	
	public boolean hasPrevious() {
		return previous != null;
	}
	
	public boolean hasNext() {
		return next != null;
	}
	
	public MyList<E> last() {
		MyList<E> here = this;
		
		while (here.hasNext()) {
			here = here.next;
		}
		
		return here;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		if (!isEmpty()) {
			sb.append(element);
		}
		
		MyList<E> here = this;
		
		while (here.hasNext()) {
			here = here.next;
			sb.append(" " + here.element);

		}
		
		return sb.toString();
	}

	@Override
	public Iterator<E> iterator() {
		return new MyIterator(this);
	}
	
	private class MyIterator implements Iterator<E> {
		private MyList<E> here;
		
		public MyIterator(MyList<E> list) {
			here = list;
		}
		
		@Override
		public boolean hasNext() {
			return here != null && !here.isEmpty();
		}

		@Override
		public E next() {
			E item = here.element;
			here = here.next;
			return item;
		}
	}
}
